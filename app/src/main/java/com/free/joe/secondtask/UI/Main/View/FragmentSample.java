package com.free.joe.secondtask.UI.Main.View;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;

import com.free.joe.secondtask.UI.Base.BaseActivity;
import com.free.joe.secondtask.UI.Main.ViewModel.MainActivityViewModel;
import com.free.joe.secondtask.databinding.ActivityFragmentSampleBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class FragmentSample extends BaseActivity {

    private ActivityFragmentSampleBinding binding;
    private MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFragmentSampleBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        setupView();
       // viewModel.getPosts();
        //shahsdafkka

    }

    private void setupView() {
        setupViewPager();
        binding.tabLayout.setupWithViewPager(binding.viewPager);

    }

    private void setupViewPager() {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
       // adapter.addFragment(new VisitsFragment(),"Visits");
      //  adapter.addFragment(new RepresentativesFragment(),"Representatives");
        binding.viewPager.setAdapter(adapter);

    }


    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }







    @Override
    public void initViewModel() {

        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);

    }



}
