package com.free.joe.secondtask.Repository.DataProviders.Base;


import com.free.joe.secondtask.Repository.Server.ResponseBody.Base.Result;

public interface OnDataProviderResponseListener<T> {

    void onSuccess(T response);

    void onError(Result result);
}
