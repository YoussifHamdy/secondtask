package com.free.joe.secondtask.UI.Splash.View;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.free.joe.secondtask.Repository.Database.Models.User.User;
import com.free.joe.secondtask.UI.Base.BaseActivity;
import com.free.joe.secondtask.UI.Login.View.LoginActiviy;
import com.free.joe.secondtask.UI.Main.View.MainActivity;
import com.free.joe.secondtask.UI.Splash.ViewModel.SplashActivityViewModel;
import com.free.joe.secondtask.databinding.ActivitySplashBinding;

public class SplashActivity extends BaseActivity {

    private ActivitySplashBinding binding;
    private SplashActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel.getUser();
        initObservers();
    }

    private void initObservers() {
        viewModel.getUserMutableLiveData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user!=null){
                    startMainActivity();
                } else {
                    startLoginActivity();
                }
            }
        });
    }

    private void startMainActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },2000);
    }

    private void startLoginActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, LoginActiviy.class);
                startActivity(intent);
                finish();
            }
        },2000);

    }

    @Override
    public void initViewModel() {
        viewModel = new ViewModelProvider(this).get(SplashActivityViewModel.class);
        setViewModel(viewModel);
       }


}
