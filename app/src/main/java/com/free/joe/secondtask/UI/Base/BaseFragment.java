package com.free.joe.secondtask.UI.Base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.free.joe.secondtask.Utilities.SharedPreferenceHelper;

import java.util.Locale;

import static com.free.joe.secondtask.UI.Base.BaseActivity.progressDialog;
import static com.free.joe.secondtask.UI.Base.BaseActivity.snackbar;


/**
 * Created by Youssif Hamdy on 12/8/2019.
 */
public class BaseFragment extends Fragment {

    public Activity context;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    public static void ShowProgress(String Title, String Message, Context context) {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(context, Title,
                        Message, true);
            }
        } else {
            progressDialog = ProgressDialog.show(context, Title,
                    Message, true);
        }
    }


    public static void HideProgress() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }


/*    public static void ShowMovingBillSnackbar(View parentView , MovingBill movingBill, TablesViewModel mViewModel, Context context) {
        if (snackbar != null) {
            if (!snackbar.isShown()) {
                snackbar.setText(context.getString(R.string.moving_bill_no) +" " +movingBill.getBILL_NO() +" "+ context.getString(R.string.from_table_no) +" " + movingBill.getTBL_NO() +","
                        + context.getString(R.string.hall_no) +" "+ movingBill.getHALL_NO());
                snackbar.setAction(R.string.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewModel.setMovingBill(null);
                        snackbar.dismiss();
                    }
                });
                snackbar.setActionTextColor(context.getResources().getColor(R.color.colorAccent2));
                snackbar.show();
            }
        } else {
            snackbar = Snackbar.make(parentView , context.getString(R.string.moving_bill_no) +" " +movingBill.getBILL_NO() +" "+ context.getString(R.string.from_table_no) +" " + movingBill.getTBL_NO() +","
                    + context.getString(R.string.hall_no) +" "+ movingBill.getHALL_NO(), Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction(R.string.cancel, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewModel.setMovingBill(null);
                    snackbar.dismiss();
                }
            });
            snackbar.setActionTextColor(context.getResources().getColor(R.color.colorAccent2));
            snackbar.show();
        }

    }*/

    public static void HideSnackbar() {
        if (snackbar != null) {
            if (snackbar.isShown()) {
                snackbar.dismiss();
            }
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public String getLanguageId() {
        String Language = SharedPreferenceHelper.getSharedPreferenceString(context, SharedPreferenceHelper.LANGUAGE_KEY, Locale.getDefault().getLanguage());
        if (Language.equalsIgnoreCase("ar")) {
            return "1";
        }
        if (Language.equalsIgnoreCase("en")) {
            return "2";
        }

        return "1";
    }

}

