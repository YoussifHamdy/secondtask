package com.free.joe.secondtask.Utilities;

/**
 * Created by Youssif Hamdy on 12/8/2019.
 */
public class CommonMethods {/*

    public static boolean isArrayEmpty(List<Object> objectList) {
        if (objectList == null || objectList.size() == 0) {
            return true;
        }
        return false;
    }

    public static String DecodePass(String Pass) {
        int P_len = 0;
        int Cur_Chr = 0;
        String p_pass = "";
        for (int i = 0; i < Pass.length(); i++) {
            Cur_Chr = String.valueOf(Pass.charAt(i)).codePointAt(0);
            Cur_Chr = Cur_Chr - Pass.length();
            p_pass = p_pass + (char) Cur_Chr;
        }
        return p_pass;
    }

    public static float convertDpToPixel(float dp, Context context) {

        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;

    }

    public static String getDate(String dFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                dFormat, Locale.ENGLISH);
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getManufacturerSerialNumber(Context context) {

        if (IsDemo) {
            return "352419093095327";
        }

        String identifier = null;
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null)
                identifier = tm.getDeviceId();
            if (identifier == null || identifier.length() == 0)
                identifier = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (SecurityException ignored) {
            Log.d("SerialNumber", ignored.getMessage());
        }
        return identifier;
    }


    public static String getDeviceSerial(Context context) {
        String identifier = "0000000000000000";

        if (IsDemo) {
            return "352419093095327";
        }

        if (Build.VERSION.SDK_INT >= 29) {

            String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

            String uniquePseudoID = "35" +
                    Build.BOARD.length() % 10 +
                    Build.BRAND.length() % 10 +
                    Build.DEVICE.length() % 10 +
                    Build.DISPLAY.length() % 10 +
                    Build.HOST.length() % 10 +
                    Build.ID.length() % 10 +
                    Build.MANUFACTURER.length() % 10 +
                    Build.MODEL.length() % 10 +
                    Build.PRODUCT.length() % 10 +
                    Build.TAGS.length() % 10 +
                    Build.TYPE.length() % 10 +
                    Build.USER.length() % 10;
            String serial = Build.getRadioVersion() + android_id;
            String uuid = new UUID(uniquePseudoID.hashCode(), serial.hashCode()).toString();
            // Log.d("Device ID",uuid);
            return uuid;

        }


        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null)
                identifier = tm.getDeviceId();
            if (identifier == null || identifier.length() == 0)
                identifier = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception ignored) {


            // Log.d("SerialNumber", ignored.getMessage());
        }

        //return  "99000444779733";

        return identifier;

    }


    public static boolean checkResponse(ResponseObject responseObject) {

        if (responseObject == null) {
            return false;
        }
        if (responseObject.getResult() == null) {
            return false;
        }
        return responseObject.getResult().getErrNo() == 0;
    }


    public static void sendRequestToServer(LifecycleOwner lifecycleOwner, BasicViewModel basicViewModel, RequestObject requestObject, final CommonInterface commonInterface) {


        MutableLiveData<ResponseObject> mutableLiveData = new MutableLiveData<>();
        mutableLiveData.observe(lifecycleOwner, new Observer<ResponseObject>() {
            @Override
            public void onChanged(ResponseObject responseObject) {
                commonInterface.responseFromServer(responseObject);
            }
        });


        basicViewModel.request(requestObject, mutableLiveData);


    }

    public static String getImageURI(String I_IMG) {

        return "";
    }

    public static void getDataFrmDatabase(final int RequestType, LifecycleOwner lifecycleOwner, BasicViewModel basicViewModel, final CommonInterface commonInterface) {

        MutableLiveData<Object> objectMutableLiveData = new MutableLiveData<>();

        objectMutableLiveData.observe(lifecycleOwner, new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                commonInterface.responseFromDatabase(RequestType, o);
            }
        });

        MutableLiveData<List<Object>> listOfObjectMutableLiveData = new MutableLiveData<>();
        listOfObjectMutableLiveData.observe(lifecycleOwner, new Observer<List<Object>>() {
            @Override
            public void onChanged(List<Object> objectList) {
                commonInterface.responseFromDatabase(RequestType, objectList);
            }
        });


        switch (RequestType) {

            case Constants.DB_TRMNL_CODE:
                basicViewModel.getTerminalsFrmDb(objectMutableLiveData);
                break;

            case Constants.DB_BRNCH_CODE:
                basicViewModel.getBranchFrmDb(objectMutableLiveData);
                break;

            case Constants.DB_USER_CODE:
                basicViewModel.getUserFrmDb(objectMutableLiveData);
                break;

            case Constants.DB_SRVER_CODE:
                basicViewModel.getServerFrmDb(objectMutableLiveData);
                break;

            case Constants.DB_PRAM_CODE:
                basicViewModel.getPramFrmDb(objectMutableLiveData);
                break;


        }


    }

    public static String round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return String.valueOf(bd.doubleValue());

    }

    public static void showYesNoMessage(Context context, String title, String message, String positive, String negative, DialogInterface.OnClickListener onClickListener) {

        MaterialAlertDialogBuilder builder =
                new MaterialAlertDialogBuilder(context, R.style.AlertDialogStyle);

        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);
        // builder.setPositiveButton(R.string.btn_yes, onClickListener);
        builder.setPositiveButton(positive, onClickListener);
        //  builder.setNegativeButton(context.getString(R.string.btn_no), (dialog, which) -> dialog.dismiss());
        builder.setNegativeButton(negative, (dialog, which) -> dialog.dismiss());
        builder.show();
    }


    public static void calculateItmTax(FoodItem foodItem, String USE_VAT, String USE_PRICE_INCLUDE_VAT) {
        if (USE_VAT.equalsIgnoreCase("1")) {
            Double taxPercentage = Double.parseDouble(foodItem.getITM_TAX_PRCNT());
            if (USE_PRICE_INCLUDE_VAT.equalsIgnoreCase("1")) {// السعر شامل الضريبة
                Double I_PRICE_VAT = Double.parseDouble(foodItem.getI_PRICE());
                Double price = I_PRICE_VAT / ((taxPercentage / 100) + 1);
                Double vatAmnt = I_PRICE_VAT - price;
                setPriceAndTax(foodItem, price, I_PRICE_VAT, vatAmnt);
            } else {
                if (taxPercentage > 0) {//الصنف عليه ضريبه
                    Double Price = Double.parseDouble(foodItem.getI_PRICE());
                    Double vatAmnt = Price * (taxPercentage / 100);
                    setPriceAndTax(foodItem, Price, 0.0, vatAmnt);
                } else {
                    Double Price = Double.parseDouble(foodItem.getI_PRICE());
                    setPriceAndTax(foodItem, Price, 0.0, 0.0);
                }
            }
        } else {
            Double Price = Double.parseDouble(foodItem.getI_PRICE());
            setPriceAndTax(foodItem, Price, 0.0, 0.0);
        }
    }

    public static void setPriceAndTax(FoodItem foodItem, Double IPrice, Double IPriceVAT, Double VATAmt) {
        foodItem.setI_PRICE_VAT(String.valueOf(IPriceVAT));
        foodItem.setI_PRICE(String.valueOf(IPrice));
        foodItem.setVAT_AMT(String.valueOf(VATAmt));
    }


    public static Double getFoodItemWithTax(FoodItem foodItem) {
        Double Price = Double.parseDouble(foodItem.getI_PRICE());
        Double vatAmnt = Double.parseDouble(foodItem.getVAT_AMT());
        return (Price + vatAmnt);
    }

    public static String round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return String.valueOf(bd.doubleValue());

    }

*/
}
