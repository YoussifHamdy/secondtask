package com.free.joe.secondtask.UI.Signup.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.free.joe.secondtask.Repository.DataProviders.Base.OnDataProviderResponseListener;
import com.free.joe.secondtask.Repository.Server.RequestBody.Signup.SignupBody;
import com.free.joe.secondtask.Repository.Server.ResponseBody.Base.Result;
import com.free.joe.secondtask.Repository.Server.ResponseBody.Signup.SignupResponse;
import com.free.joe.secondtask.UI.Base.BaseViewModel;

public class SignupViewModel extends BaseViewModel {
    public SignupViewModel(@NonNull Application application) {
        super(application);
    }

    public void signup (String fullName, String email, int phoneNumber, String password){

        SignupBody signupBody = new SignupBody();
        signupBody.setFullname(fullName);
        signupBody.setEmail(email);
        signupBody.setPassword(password);
        signupBody.setPhone(phoneNumber);

        getUserDataProvider().signup(signupBody, new OnDataProviderResponseListener<SignupResponse>() {
            @Override
            public void onSuccess(SignupResponse response) {

            }

            @Override
            public void onError(Result result) {

            }
        });
    }
}
