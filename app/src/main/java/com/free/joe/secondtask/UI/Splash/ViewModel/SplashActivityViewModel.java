package com.free.joe.secondtask.UI.Splash.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;


import com.free.joe.secondtask.Repository.DataProviders.Base.OnDataProviderResponseListener;
import com.free.joe.secondtask.Repository.Database.Models.User.User;
import com.free.joe.secondtask.Repository.Server.ResponseBody.Base.Result;
import com.free.joe.secondtask.UI.Base.BaseViewModel;

public class SplashActivityViewModel extends BaseViewModel {

    private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();

    public SplashActivityViewModel(@NonNull Application application) {
        super(application);
    }


    public void getUser() {
        getUserDataProvider().getUser(new OnDataProviderResponseListener<User>() {
            @Override
            public void onSuccess(User user) {

                userMutableLiveData.postValue(user);
            }

            @Override
            public void onError(Result result) {

            }
        });

    }

    public MutableLiveData<User> getUserMutableLiveData() {
        return userMutableLiveData;
    }
}
