package com.free.joe.secondtask.UI.Base;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;


import com.free.joe.secondtask.Application.LocaleHelper;
import com.free.joe.secondtask.Utilities.NetworkUtil;
import com.free.joe.secondtask.Utilities.SharedPreferenceHelper;
import com.google.android.material.snackbar.Snackbar;

import org.greenrobot.eventbus.EventBus;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

/**
 * Created by Youssif Hamdy on 12/8/2019.
 */
public abstract class BaseActivity extends AppCompatActivity {


    public static ProgressDialog progressDialog;
    public static Snackbar snackbar;
    BroadcastReceiver networkReceiver;
    BaseViewModel viewModel;

    Toolbar toolbar;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.setLocale(newBase));

    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 25) {
            createConfigurationContext(overrideConfiguration);
        }
        super.applyOverrideConfiguration(getResources().getConfiguration());
    }


    public Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseActivity.this;
        setConnectionStatus();
        initViewModel();

    }

    public abstract void initViewModel();

    public void setViewModel(BaseViewModel viewModel) {
        this.viewModel = viewModel;
    }

    private void setConnectionStatus() {

        networkReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = NetworkUtil.getConnectivityStatusString(context);
                if (status == NetworkUtil.TYPE_NOT_CONNECTED) {
                    viewModel.setIsConnectToInternet(false);
                } else {
                    viewModel.setIsConnectToInternet(true);
                }

            }
        };

    }





/*

    public void DefineToolbar(String Title) {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(Title);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity.this.finish();
            }
        });

    }*/

    public static void ShowProgress(String Title, String Message, Context context) {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(context, Title,
                        Message, true);
            }
        } else {
            progressDialog = ProgressDialog.show(context, Title,
                    Message, true);
        }
    }


    public static void HideProgress() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    public String getLanguageId() {
        String Language = SharedPreferenceHelper.getSharedPreferenceString(BaseActivity.this, SharedPreferenceHelper.LANGUAGE_KEY, Locale.getDefault().getLanguage());
        if (Language.equalsIgnoreCase("ar")) {
            return "1";
        }
        if (Language.equalsIgnoreCase("en")) {
            return "2";
        }

        return "1";
    }






}
