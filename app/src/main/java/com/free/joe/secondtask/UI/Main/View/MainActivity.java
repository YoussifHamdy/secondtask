package com.free.joe.secondtask.UI.Main.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.free.joe.secondtask.UI.Base.BaseActivity;
import com.free.joe.secondtask.UI.Main.ViewModel.MainActivityViewModel;
import com.free.joe.secondtask.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());



    }

    @Override
    public void initViewModel() {
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        setViewModel(viewModel);
    }
}
