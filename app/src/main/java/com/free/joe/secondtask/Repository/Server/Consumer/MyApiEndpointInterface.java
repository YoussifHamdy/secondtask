package com.free.joe.secondtask.Repository.Server.Consumer;

import com.free.joe.secondtask.Repository.Server.RequestBody.Login.LoginBody;
import com.free.joe.secondtask.Repository.Server.RequestBody.Signup.SignupBody;
import com.free.joe.secondtask.Repository.Server.ResponseBody.LoginResponse.LoginResponse;
import com.free.joe.secondtask.Repository.Server.ResponseBody.Signup.SignupResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Abubaker on 28/06/2016.
 */

public interface MyApiEndpointInterface {

//        Call<ResponseObject> call = apiService.LoginActivity(type, year, fnYear, String.valueOf(LangId), branchNo, userNo, password);
/*
    @GET("CheckLogIn?")
    Call<LoginResponse> Login(@Query("type") String type,
                              @Query("year") int year,
                              @Query("activityNumber") int fnYear,
                              @Query("languageID") int languageID,
                              @Query("branchNumber") int BranchNo,
                              @Query("userID") int userID,
                              @Query("userPassword") String Password);*/

    @POST("user/login")
    Call<LoginResponse> Login(@Body LoginBody loginBody);

    @POST("user/signup")
    Call<SignupResponse> Signup(@Body SignupBody signupBody);





}

