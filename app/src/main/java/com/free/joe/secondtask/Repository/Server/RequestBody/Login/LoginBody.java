package com.free.joe.secondtask.Repository.Server.RequestBody.Login;

import com.google.gson.annotations.SerializedName;

public class LoginBody {
    @SerializedName("user_phone")
    private String userName;

    @SerializedName("login_password")
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
