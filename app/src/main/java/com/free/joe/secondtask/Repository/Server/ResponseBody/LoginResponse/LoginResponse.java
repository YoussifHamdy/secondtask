package com.free.joe.secondtask.Repository.Server.ResponseBody.LoginResponse;

import com.free.joe.secondtask.Repository.Server.ResponseBody.Base.ResponseObject;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends ResponseObject {

    @SerializedName("Data")
    LoginData loginData;

    public LoginData getLoginData() {
        return loginData;
    }
}
