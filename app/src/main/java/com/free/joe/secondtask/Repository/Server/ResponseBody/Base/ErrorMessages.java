package com.free.joe.secondtask.Repository.Server.ResponseBody.Base;

import android.content.Context;


/**
 * Created by Youssif Hamdy on 3/22/2020.
 */
public class ErrorMessages {


    public static String getDefaultMessage(Context context, Result result) {
        int Errorcode = result.getErrorNumber();
        switch (Errorcode) {

            case 10:
                return result.getErrorMessage();

            case 100:
                return result.getErrorMessage();

            default:
                return result.getErrorMessage();

        }
    }


}
