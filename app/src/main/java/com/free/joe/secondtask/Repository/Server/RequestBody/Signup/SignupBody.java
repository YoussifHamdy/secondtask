package com.free.joe.secondtask.Repository.Server.RequestBody.Signup;

import com.google.gson.annotations.SerializedName;

import java.security.SecureRandom;

public class SignupBody {

    @SerializedName("phone")
    private int phone;

    @SerializedName("email")
    private String email;

    @SerializedName("firstName")
    private String fullname;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("password")
    private String password;

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
