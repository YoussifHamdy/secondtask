package com.free.joe.secondtask.Repository.DataProviders.User;


import com.free.joe.secondtask.Repository.DataProviders.Base.BaseDataProvider;
import com.free.joe.secondtask.Repository.DataProviders.Base.OnDataProviderResponseListener;
import com.free.joe.secondtask.Repository.Database.Models.User.User;
import com.free.joe.secondtask.Repository.Database.Models.User.UserDao;
import com.free.joe.secondtask.Repository.Server.RequestBody.Login.LoginBody;
import com.free.joe.secondtask.Repository.Server.RequestBody.Signup.SignupBody;
import com.free.joe.secondtask.Repository.Server.ResponseBody.Signup.SignupResponse;
import com.free.joe.secondtask.Repository.Server.ResponseBody.Base.Result;
import com.free.joe.secondtask.Repository.Server.ResponseBody.LoginResponse.LoginResponse;

public class UserDataProvider extends BaseDataProvider<UserDao> {
    public static UserDataProvider sharedInstance = new UserDataProvider();


    @Override
    public UserDao getDao() {
        return this.mDb.userDao();
    }

    public void login(LoginBody loginBody, OnDataProviderResponseListener<LoginResponse> responseListener) {

        webServiceConsumer.callService(webServiceConsumer.getAPI().Login(loginBody), new OnDataProviderResponseListener<LoginResponse>() {
            @Override
            public void onSuccess(LoginResponse response) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        User user = new User();
                        user.setUserName(response.getLoginData().getUserName());
                        user.setFirstName(response.getLoginData().getFirstName());
                        user.setLastName(response.getLoginData().getLastName());
                        user.setEmail(response.getLoginData().getEmail());
                        user.setPhone(response.getLoginData().getPhone());
                        user.setUserCode(response.getLoginData().getUserCode());

                        //getDao().deleteAll();
                        getDao().insert(user);
                    }
                });

               responseListener.onSuccess(response);
            }

            @Override
            public void onError(Result result) {
                responseListener.onError(result);
            }
        });
    }

    public void signup(SignupBody signupBody, OnDataProviderResponseListener<SignupResponse> responseListener) {

        webServiceConsumer.callService(webServiceConsumer.getAPI().Signup(signupBody), new OnDataProviderResponseListener<SignupResponse>() {
            @Override
            public void onSuccess(SignupResponse response) { responseListener.onSuccess(response); }
            @Override
            public void onError(Result result) { responseListener.onError(result); }
        });
    }


    public void getUser(OnDataProviderResponseListener<User> userOnDataProviderResponseListener) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                userOnDataProviderResponseListener.onSuccess(getDao().getUser());
            }
        });
    }
}

