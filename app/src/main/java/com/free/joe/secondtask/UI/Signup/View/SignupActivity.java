package com.free.joe.secondtask.UI.Signup.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.free.joe.secondtask.R;
import com.free.joe.secondtask.UI.Base.BaseActivity;
import com.free.joe.secondtask.UI.Login.View.LoginActiviy;
import com.free.joe.secondtask.UI.Signup.ViewModel.SignupViewModel;
import com.free.joe.secondtask.databinding.ActivitySignupBinding;

public class SignupActivity extends BaseActivity {

    private ActivitySignupBinding binding;
    private SignupViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignupBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initListener();
    }

    private void initListener() {
        binding.signupBotton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkfiled()){
                    viewModel.signup(binding.fullname.getText().toString(), binding.emailAddresss.getText().toString(),
                            binding.mobileNumber.getText().length(), binding.passwordSignup.getText().toString());
                }
            }
        });

        binding.loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActiviy.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean checkfiled() {
        if (binding.fullname.getText().toString().isEmpty()){
            binding.fullname.setError("please enter fullname");
            binding.fullname.requestFocus();
            return false;
        }
        if (binding.emailAddresss.getText().toString().isEmpty()){
            binding.emailAddresss.setError("please enter EmailAddress");
            binding.emailAddresss.requestFocus();
        }
        if (binding.mobileNumber.getText().toString().isEmpty()){
            binding.mobileNumber.setError("please enter mobileNumber");
            binding.mobileNumber.requestFocus();
        }
        if (binding.passwordSignup.getText().toString().isEmpty()){
            binding.passwordSignup.setError("please enter password");
            binding.passwordSignup.requestFocus();
        }
        if (binding.passwordConfirm.getText().toString().isEmpty()){
            binding.passwordConfirm.setError("please enter password");
            binding.passwordConfirm.requestFocus();
        }
        return true;
    }


    @Override
    public void initViewModel() {
        viewModel = new ViewModelProvider(this).get(SignupViewModel.class);
        setViewModel(viewModel);
    }
}
