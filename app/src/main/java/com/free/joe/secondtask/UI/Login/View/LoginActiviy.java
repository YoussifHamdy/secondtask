package com.free.joe.secondtask.UI.Login.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.free.joe.secondtask.R;
import com.free.joe.secondtask.UI.Base.BaseActivity;
import com.free.joe.secondtask.UI.Login.ViewModel.LoginViewModel;
import com.free.joe.secondtask.UI.Main.View.MainActivity;
import com.free.joe.secondtask.UI.Signup.View.SignupActivity;
import com.free.joe.secondtask.databinding.ActivityLoginActiviyBinding;

public class LoginActiviy extends BaseActivity {

    private ActivityLoginActiviyBinding binding;
    private LoginViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginActiviyBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initObservers();
        initListener();
    }

    private void initObservers() {
        viewModel.getLoginMLD().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Toast.makeText(LoginActiviy.this, "login success", Toast.LENGTH_SHORT).show();
                startMainActivity();
            }
        });
    }

    private void startMainActivity() {
        Intent intent = new Intent(LoginActiviy.this , MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void initListener() {
        binding.Buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkFiles()){
                    viewModel.login(binding.mobileNumber.getText().toString() , binding.password.getText().toString());
                }
            }
        });
        binding.singUpClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActiviy.this, SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean checkFiles() {
        if (binding.mobileNumber.getText().toString().isEmpty()){
            binding.mobileNumber.setError(getString(R.string.check));
            binding.mobileNumber.requestFocus();
            return false;
        }
        if (binding.password.getText().toString().isEmpty()){
            binding.password.setError(getString(R.string.check_password));
            binding.password.requestFocus();
            return false;

        }
        return true;
    }

    @Override
    public void initViewModel() {
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        setViewModel(viewModel);


    }

}
