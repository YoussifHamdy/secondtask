package com.free.joe.secondtask.Repository.Server.ResponseBody.LoginResponse;

import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("userName")
    String userName;

    @SerializedName("firstName")
    String firstName;

    @SerializedName("lastName")
    String lastName;

    @SerializedName("email")
    String email;

    @SerializedName("phone")
    String phone;

    @SerializedName("userCode")
    String userCode;

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getUserCode() {
        return userCode;
    }
}
