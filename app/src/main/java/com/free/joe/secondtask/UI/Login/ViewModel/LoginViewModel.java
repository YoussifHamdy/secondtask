package com.free.joe.secondtask.UI.Login.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.free.joe.secondtask.Repository.DataProviders.Base.OnDataProviderResponseListener;
import com.free.joe.secondtask.Repository.Server.RequestBody.Login.LoginBody;
import com.free.joe.secondtask.Repository.Server.ResponseBody.Base.Result;
import com.free.joe.secondtask.Repository.Server.ResponseBody.LoginResponse.LoginResponse;
import com.free.joe.secondtask.UI.Base.BaseViewModel;

public class LoginViewModel extends BaseViewModel {

    private MutableLiveData<Boolean> loginMLD = new MutableLiveData<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    public void login (String username, String password){

        LoginBody loginBody = new LoginBody();
        loginBody.setUserName(username);
        loginBody.setPassword(password);



        getUserDataProvider().login(loginBody, new OnDataProviderResponseListener<LoginResponse>() {
            @Override
            public void onSuccess(LoginResponse response) {
                loginMLD.postValue(true);
            }

            @Override
            public void onError(Result result) {

            }
        });
    }

    public MutableLiveData<Boolean> getLoginMLD() {
        return loginMLD;
    }
}
