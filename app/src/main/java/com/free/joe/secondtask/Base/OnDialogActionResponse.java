package com.free.joe.secondtask.Base;

public interface OnDialogActionResponse<T> {

    void onPositiveButton(T response);

    void onNegativeButton();
}
